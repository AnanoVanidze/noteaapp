package com.example.notes

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.note_item_layout.*

class MainActivity : AppCompatActivity() {

    private var notesAdapter = NotesAdapter()
    private val REQUEST_CODE = 1
    private var result: Note? = null
    private var notesArray = mutableListOf<Note>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        notesRecyclerView.layoutManager = LinearLayoutManager(this)
        notesRecyclerView.adapter = notesAdapter
        notesAdapter.noteItems.addAll(notesArray)
        notesAdapter.notifyDataSetChanged()
    }

    private fun init() {


        addButton.setOnClickListener() {
            val intent = Intent(this, AddNotesActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE)
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE)
            if (resultCode == Activity.RESULT_OK) {
                result = data?.getParcelableExtra("note")
                result?.let { notesAdapter.noteItems.add(addNote(it)) }
                notesAdapter.notifyDataSetChanged()
            } else
                Toast.makeText(this, "note was not saved successfully", Toast.LENGTH_SHORT).show()
    }

    private fun addNote(note: Note): Note {
        return note
    }

}