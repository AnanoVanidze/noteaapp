package com.example.notes

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize

@Parcelize
class Note(val title:String, val description:String): Parcelable

