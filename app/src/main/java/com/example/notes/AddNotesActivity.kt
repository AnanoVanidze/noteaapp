package com.example.notes

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_enter_notes.*

class AddNotesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_notes)



        saveButton.setOnClickListener {
            sendData()
            saveNote()
        }
    }


    private fun saveNote() {

        val returnIntent = Intent()
        val note = Note(titleET.text.toString(), notesET.text.toString())
        returnIntent.putExtra("note", note)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
    private fun sendData(){
        if (titleET.text.toString().isNotEmpty() && notesET.text.toString().isNotEmpty()){
            val database = Firebase.database
            val myRef = database.getReference(titleET.text.toString())
            myRef.setValue(notesET.text.toString())
        }

    }

}