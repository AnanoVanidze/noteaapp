package com.example.notes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_authentication_activity.*
import kotlinx.android.synthetic.main.activity_authentication_activity.signUpButton
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_in.emailET
import kotlinx.android.synthetic.main.activity_sign_in.passwordET
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init() {
        auth = Firebase.auth
        signUp.setOnClickListener {
            signUp()

        }

    }
    private fun signUp() {
        val email = emailET.text.toString()
        val password = passwordET.text.toString()
        val repeatPassword = repeatPasswordET.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                signUp.isClickable = false
                if (email.matches(emailPattern.toRegex())) {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            signUp.isClickable = true
                            if (task.isSuccessful) {
                                d("signUp", "createUserWithEmail:success")
                                Toast.makeText(this, "SignUp is success!", Toast.LENGTH_SHORT)
                                    .show()
                                val intent = Intent(this,MainActivity::class.java)
                                startActivity(intent)
                                val user = auth.currentUser

                            } else {
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(
                                    this, task.exception.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                        }
                }else {
                    Toast.makeText(applicationContext, "Email format is not Correct",
                        Toast.LENGTH_SHORT).show()
                }



            } else {
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show()
        }

    }

}